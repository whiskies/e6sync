Requirements:
- Python 3.x
- requests (pip3 install requests)

```
Usage: e6sync.py [-h] tags dir

positional arguments:
  tags        tags to query
  dir         target directory

optional arguments:
  -h, --help  show this help message and exit
  
Examples:
  Download all pictures of foxes with a score greater than 25:
    python e6sync.py 'fox score:>25' ~/Downloads/foxes
  Download all pictures favourited by the user whiskies:
    python e6sync.py 'fav:whiskies' ~/Pictures/gayfurryshit
```