import sys
import math
import argparse
import os
import requests


class Image:
    def __init__(self, url, name, sum, size):
        self.__url = url
        self.__name = name
        self.__sum = sum
        self.__size = size

    def exists(self, dir):
        return os.path.isfile(os.path.join(dir, self.__name))

    def fetch(self, dir):
        res = requests.get(self.__url)
        if res.status_code == 200:
            with open(os.path.join(dir, self.__name), 'wb') as f:
                f.write(res.content)
            return True
        return False

    def name(self):
        return self.__name

    def size(self):
        # convert bytes to kilobytes
        return math.floor(self.__size * 0.001)


def parse_tags(url):
    # api can be flakey, try 5 times to workaround this
    # site will not accept requests unless a valid user agent string is set
    user_agent = {'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0'}
    for i in range(0, 5):
        res = requests.get(url, headers = user_agent)
        if res.status_code == 200:
            j = res.json()
            break
        elif i == 5:
            return -1

    data = []
    l = j.__len__()

    for i in range(0, l):
        data.append(Image(j[i]['file_url'], j[i]['md5'] + '.' + j[i]['file_ext'], j[i]['md5'], j[i]['file_size']))

    return data

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('tags', help='tags to query')
    p.add_argument('dir', help='target directory')
    args = p.parse_args()

    tags = args.tags.strip('\'')
    tags = tags.strip('"')

    if not os.path.exists(args.dir):
        print('info: target directory doesn\'t exist, creating...')
        os.makedirs(args.dir)

    q = '?tags=' + requests.utils.quote(tags) + '&limit=64&page='
    u = 'https://e621.net/post/index.json'

    page = 1
    images = []
    while True:
        r = parse_tags(u + q + str(page))
        if r == -1:
            print('err: api query failed')
            sys.exit(1)
        elif not r:
            break
        images += r
        page += 1

    transmitted = 0
    downloaded = 0
    skipped = 0
    failed = 0
    total = 0

    for image in images:
        if not image.exists(args.dir):
            print('Downloading: {}...\r'.format(image.name()), end='')
            if image.fetch(args.dir):
                downloaded += 1
                transmitted += image.size()
            else:
                failed += 1
                print('warn: failed to download {}'.format(image.name()))
        else:
            skipped += 1
        total += 1

    print('Session statistics: {}/{} images downloaded ({} failed, {} skipped), {} kbs transferred'.format(downloaded,
                                                                                                           total,
                                                                                                           failed,
                                                                                                           skipped,
                                                                                                           transmitted))

    sys.exit(0)
